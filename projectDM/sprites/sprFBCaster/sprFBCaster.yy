{
    "id": "424b981f-790a-4f6b-9a94-0f1b7e9ca092",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFBCaster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 8,
    "bbox_right": 52,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06200f20-6e3e-4f1a-b0cb-f85897d07556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "424b981f-790a-4f6b-9a94-0f1b7e9ca092",
            "compositeImage": {
                "id": "a3105a36-a68b-4857-9422-74b1700f47ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06200f20-6e3e-4f1a-b0cb-f85897d07556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d0b5bf-459b-498f-bc38-ecdf9d59f88b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06200f20-6e3e-4f1a-b0cb-f85897d07556",
                    "LayerId": "e5652163-b9a2-4e0a-a741-df3263a0e9e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e5652163-b9a2-4e0a-a741-df3263a0e9e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "424b981f-790a-4f6b-9a94-0f1b7e9ca092",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}