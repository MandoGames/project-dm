{
    "id": "24f9fdef-c416-490b-9f61-8669c7d72d52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFBCaster",
    "eventList": [
        {
            "id": "4cbde8f7-8853-4679-8959-afd0066b72fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24f9fdef-c416-490b-9f61-8669c7d72d52"
        },
        {
            "id": "2de1f367-14ff-4403-a11a-6113cb844b08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24f9fdef-c416-490b-9f61-8669c7d72d52"
        },
        {
            "id": "0d192308-2d7f-4f6c-ab1f-66d459c5c59e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "24f9fdef-c416-490b-9f61-8669c7d72d52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "424b981f-790a-4f6b-9a94-0f1b7e9ca092",
    "visible": true
}