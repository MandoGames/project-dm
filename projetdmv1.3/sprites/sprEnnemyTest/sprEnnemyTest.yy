{
    "id": "d63b8638-9be3-4323-899c-6eb6d58a3dc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprEnnemyTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d51d3b83-2a0c-41fa-933b-d309ed01dfe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d63b8638-9be3-4323-899c-6eb6d58a3dc5",
            "compositeImage": {
                "id": "419b0c3b-7f29-4ada-b5f7-8799dcb57c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51d3b83-2a0c-41fa-933b-d309ed01dfe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92e580e8-5110-42f5-8b04-1cfc55fd5134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51d3b83-2a0c-41fa-933b-d309ed01dfe2",
                    "LayerId": "18334c21-5018-4b66-a731-84c38b791f15"
                }
            ]
        },
        {
            "id": "46b06f00-5da5-471f-969e-60cb03857696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d63b8638-9be3-4323-899c-6eb6d58a3dc5",
            "compositeImage": {
                "id": "8a6d433b-702a-4577-8a0d-cd65ae75327d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b06f00-5da5-471f-969e-60cb03857696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0489613b-94f7-4e1f-9a35-d40328bcd71d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b06f00-5da5-471f-969e-60cb03857696",
                    "LayerId": "18334c21-5018-4b66-a731-84c38b791f15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "18334c21-5018-4b66-a731-84c38b791f15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d63b8638-9be3-4323-899c-6eb6d58a3dc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}