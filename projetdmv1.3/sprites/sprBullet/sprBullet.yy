{
    "id": "6b99445f-c212-420a-8cf1-3d1bc4a38bfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29392b05-3b58-4c0d-bb5d-53c271c25936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b99445f-c212-420a-8cf1-3d1bc4a38bfc",
            "compositeImage": {
                "id": "ef074526-45a9-4cf1-9378-4f569b717e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29392b05-3b58-4c0d-bb5d-53c271c25936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15cbe574-ecc5-41dc-97ec-04a26b090528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29392b05-3b58-4c0d-bb5d-53c271c25936",
                    "LayerId": "52383c89-5cd2-4873-b7f2-b6c91e940150"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52383c89-5cd2-4873-b7f2-b6c91e940150",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b99445f-c212-420a-8cf1-3d1bc4a38bfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}