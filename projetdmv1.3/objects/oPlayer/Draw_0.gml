draw_self();
//CLSN LINE
if (myDirection == 0) 
{
	draw_line(x, y, x + 50, y);
}
if (myDirection == 90) 
{
	draw_line(x, y, x, y - 50);
}
if (myDirection == 180) 
{
	draw_line(x, y, x - 50, y);
}
if (myDirection == 270) 
{
	draw_line(x, y, x, y + 50);
}

//CLSN SHIELD
if (resetShield)
{
draw_circle(x, y, 125, true);
}

//CLSN AUTOLOCK
draw_circle(x, y, 200, true);