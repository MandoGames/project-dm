//RELAUNCH THE GAME
if (keyboard_check_pressed(vk_f5))
{
	game_restart();
}


// MV + CLSN + CLSN LINE

//right
if (keyboard_check(ord("D")) && place_free(x + collisionSpeed, y)) //Activation du mvmt
{
	x += mvSpeed;// Deffinition 
	collidedThing = collision_line(x, y, x+50,y , all, false, true);
	myDirection = 0;
}

//left
if (keyboard_check(ord("Q")) && place_free(x - collisionSpeed, y))
{
	x -= mvSpeed;
	collidedThing = collision_line(x, y, x - 50, y, all, false, true);
	myDirection = 180;
}

//up
if (keyboard_check(ord("Z")) && place_free(x, y - collisionSpeed))
{
	y -= mvSpeed;
	collidedThing = collision_line(x, y, x, y - 50, all, false, true);
	myDirection = 90;
}

//down
if (keyboard_check(ord("S")) && place_free(x, y + collisionSpeed))
{
	y += mvSpeed;
	collidedThing = collision_line(x, y, x, y + 50, all, false, true)
	myDirection = 270;
}


//COLLISION CIRCLE
collidedFB = collision_circle(x, y, 125, oFireBall, true, true);
if ((resetShield = true) && (collidedFB != noone))
{
	
	instance_destroy(collidedFB);
	alarm[1] = 150;
	resetShield = false;
	
}
show_debug_message(collidedThing); //Affiche a quel obj oPlayer fait face.

//TIR
//+ where shot are.

if (on_fire) && (keyboard_check(ord("E"))) //Activation de la compétence
{
	if (myDirection == 0) // Test pour savoir dans quelle direction oPlayer est
	{
		instance_create_layer(x+32 ,y,"iBullet",oBullet);//Création de l'instance oBullet
		on_fire = false;	//Empecher tir infini
		alarm[2] = 50;	// Set le temps de CD
	}
	else if (myDirection == 90)
	{
		instance_create_layer(x,y-32,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
	else if (myDirection ==180)
	{
		instance_create_layer(x-32,y,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
	else if (myDirection == 270)
	{
		instance_create_layer(x,y+32,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
}


//AUTOLOCK
if (keyboard_check(ord("R")) && (autolock_on))	//Init de la compétence
{
	listID = ds_list_create();	//Création de la liste d'ID
	num = collision_circle_list(x,y,200,oEnnemyTest,true,true,listID, true);	//Remplissage de la liste;
	if (num > nbMaxAutoLock) {num = nbMaxAutoLock;}		//Vérification du nobre max de cible	
	alarm[3]=100;	//Timer de CD de la compétence
	autolock_on = false;	// Bool pour enmpecher de reactiver la compétence instant
	repeatAutolock = true;	//Bool pour activer la repétition du ciblage
}
		

if ((i < num) && (repeatAutolock)) && (delayTargeting)	//"Boucle" pour target avec intervale
{
	currentID = ds_list_find_value(listID,i);	//Chercher valeur dans liste
	currentID.targetedEnnemy = true;	//Active l'état de ciblage + Destruction sur oEnnemyTest
	alarm[4] = 5;	//Temps entre chaque activation de la "boucle"
	i = i + 1;	//Incrémentation du compteur de la "boucle"
	delayTargeting = false;  //Bool pour empecher la réactivation en attendant que le delay soit finis
	
}
else if ((i >= num) && (repeatAutolock)) //Mettre fin a l'autolock
{	
	repeatAutolock = false;		// Empecher la reactivation
	ds_list_clear(listID);	//Vider la liste (degage de la mémoire)
	i = 0; //reset le compteur
}
