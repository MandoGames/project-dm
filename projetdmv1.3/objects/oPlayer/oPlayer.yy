{
    "id": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "e9127769-7654-45fd-9638-9eed79627979",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        },
        {
            "id": "03991d94-186f-485a-bb98-736e0a4c0062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        },
        {
            "id": "a09af4ed-750a-4a6f-8fd5-d2ca42f56dcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        },
        {
            "id": "2ba075cc-e911-4fa2-ae93-27834ebcfdc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        },
        {
            "id": "3cbf379d-8c36-44c4-8dbe-500cb7e83a18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        },
        {
            "id": "d1cd047d-3160-40d3-93db-9ae0bf6d6dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        },
        {
            "id": "742c8704-06e0-4730-94a8-4ee08a6356c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "0fd55400-0962-4be6-9ea5-9ff47c69ca0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "7ed6ed7f-a355-467e-bc85-a7220fe2eb80",
    "visible": true
}