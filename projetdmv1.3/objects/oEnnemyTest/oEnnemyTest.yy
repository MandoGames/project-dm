{
    "id": "7aa2440d-b17a-47df-a131-3aa36b054c87",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnnemyTest",
    "eventList": [
        {
            "id": "bbfd1115-b0b2-4762-b904-24d51f2a5da3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7aa2440d-b17a-47df-a131-3aa36b054c87"
        },
        {
            "id": "417fd099-1264-4763-8c76-4e23974e9630",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7aa2440d-b17a-47df-a131-3aa36b054c87"
        },
        {
            "id": "26108a60-0c7e-497a-a166-8082def92258",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7aa2440d-b17a-47df-a131-3aa36b054c87"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d63b8638-9be3-4323-899c-6eb6d58a3dc5",
    "visible": true
}