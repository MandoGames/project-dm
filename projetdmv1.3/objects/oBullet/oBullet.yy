{
    "id": "6670d78d-64f9-40c2-8e79-0a6aedb6f42b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "b7e84e7a-fa24-4672-9940-29737e15da4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6670d78d-64f9-40c2-8e79-0a6aedb6f42b"
        },
        {
            "id": "00500207-7053-4b05-8538-4eb02015fbb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "6670d78d-64f9-40c2-8e79-0a6aedb6f42b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b99445f-c212-420a-8cf1-3d1bc4a38bfc",
    "visible": true
}