// MV + COLLISION + COLLISION LINE:

//right
if (keyboard_check(ord("D")) && place_free(x + collisionSpeed, y))
{
	x += mvSpeed;
	collidedThing = collision_line(x, y, x+50,y , all, false, true);
	myDirection = 0;
}

//left
if (keyboard_check(ord("Q")) && place_free(x - collisionSpeed, y))
{
	x -= mvSpeed;
	collidedThing = collision_line(x, y, x - 50, y, all, false, true);
	myDirection = 180;
}

//up
if (keyboard_check(ord("Z")) && place_free(x, y - collisionSpeed))
{
	y -= mvSpeed;
	collidedThing = collision_line(x, y, x, y - 50, all, false, true);
	myDirection = 90;
}

//down
if (keyboard_check(ord("S")) && place_free(x, y + collisionSpeed))
{
	y += mvSpeed;
	collidedThing = collision_line(x, y, x, y + 50, all, false, true)
	myDirection = 270;
}


//COLLISION CIRCLE
collidedFB = collision_circle(x, y, 125, oFireBall, true, true);
if ((resetShield = true) && (collidedFB != noone))
{
	
	instance_destroy(collidedFB);
	alarm[1] = 150;
	resetShield = false;
	
}
show_debug_message(collidedThing);

//TIR
//+ where shot are.

if (on_fire) && (keyboard_check(ord("E")))
{
	if (myDirection == 0)
	{
		instance_create_layer(x+32 ,y,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
	else if (myDirection == 90)
	{
		instance_create_layer(x,y-32,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
	else if (myDirection ==180)
	{
		instance_create_layer(x-32,y,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
	else if (myDirection == 270)
	{
		instance_create_layer(x,y+32,"iBullet",oBullet);
		on_fire = false;
		alarm[2] = 50;
	}
}
