{
    "id": "b467954e-abfa-4f67-b6c0-fa2b8b91b16e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFireBall",
    "eventList": [
        {
            "id": "874b4590-59fb-4bc7-85a2-fd0513349f99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b467954e-abfa-4f67-b6c0-fa2b8b91b16e"
        },
        {
            "id": "e59d8f26-53ab-47cb-8136-a5e1b5ee5514",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "b467954e-abfa-4f67-b6c0-fa2b8b91b16e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2108eebb-f43d-48c6-a57b-640402cf202c",
    "visible": true
}