{
    "id": "7ed6ed7f-a355-467e-bc85-a7220fe2eb80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c33ca28e-f913-4260-9936-3d715053386f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ed6ed7f-a355-467e-bc85-a7220fe2eb80",
            "compositeImage": {
                "id": "677aae3c-204b-44e0-930b-9111a4140c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33ca28e-f913-4260-9936-3d715053386f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6170bf3b-c4d4-4b67-8824-e5e37c1474fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33ca28e-f913-4260-9936-3d715053386f",
                    "LayerId": "22204e93-8f44-4c90-a93c-ce920636fedf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "22204e93-8f44-4c90-a93c-ce920636fedf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ed6ed7f-a355-467e-bc85-a7220fe2eb80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}