{
    "id": "2108eebb-f43d-48c6-a57b-640402cf202c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFireBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1855425f-ae7c-493c-b829-99d1fec03fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2108eebb-f43d-48c6-a57b-640402cf202c",
            "compositeImage": {
                "id": "301c7682-e969-44c0-ae02-923db0196f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1855425f-ae7c-493c-b829-99d1fec03fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1197fccd-c092-4942-a23f-dc11eaca4bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1855425f-ae7c-493c-b829-99d1fec03fce",
                    "LayerId": "9003d072-ed96-4b5e-9404-777be6b2b86c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9003d072-ed96-4b5e-9404-777be6b2b86c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2108eebb-f43d-48c6-a57b-640402cf202c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}