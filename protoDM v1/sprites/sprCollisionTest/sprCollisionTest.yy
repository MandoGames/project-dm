{
    "id": "c5aaf851-8740-4d4a-a047-ae4a930c5a60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollisionTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c761ac12-699c-4b74-aa6f-4e01e51fc6ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5aaf851-8740-4d4a-a047-ae4a930c5a60",
            "compositeImage": {
                "id": "e24aea46-24e4-4f91-bdb2-3f0dc51213c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c761ac12-699c-4b74-aa6f-4e01e51fc6ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e013a36b-8382-4564-8233-993f9ffc351a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c761ac12-699c-4b74-aa6f-4e01e51fc6ff",
                    "LayerId": "289472d1-bced-42de-adb1-8169bda906ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "289472d1-bced-42de-adb1-8169bda906ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5aaf851-8740-4d4a-a047-ae4a930c5a60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}